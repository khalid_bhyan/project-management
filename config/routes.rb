require 'resque/server'
require 'resque/scheduler/server'

Rails.application.routes.draw do
  namespace :v1 do
    namespace :reports do
      get 'projects_report/show'
    end
  end

  # mount the resque server
  mount Resque::Server.new, at: '/resque'

  concern :main_api do
    # sessions & passwords routes
    resource :session, only: [:create, :destroy]
    resource :password, only: :update do
      member do
        post :forgot
        put :reset
      end
    end

    # models-controllers routes
    resource :company, except: :index
    resources :employees do
      resource :projects,
                controller: :employee_projects,
                only: :update

      resource :tasks,
                controller: :employee_tasks,
                only: :update

      post :import, on: :collection
    end

    concern :task do
      resources :tasks
    end
    resources :projects, concerns: :task do
      resource :employees,
                controller: :project_employees,
                only: :update
    end

    # reports routes
    namespace :reports do
      resource :project, controller: :projects_report, only: :show
    end
  end

  # API version namespaces
  namespace :v1 do
    concerns :main_api
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
