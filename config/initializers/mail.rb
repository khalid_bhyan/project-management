ActionMailer::Base.smtp_settings = {
  address:         'smtp.sendgrid.net',
  port:            587,
  domain:          Rails.application.secrets.host,
  authentication:  :plain,
  user_name:       Rails.application.secrets.sendgrid[:username],
  password:        Rails.application.secrets.sendgrid[:key]
}