source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0.1'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.11'

# models & arrays pagination
gem 'kaminari', '~> 1.1.1'

# models serialization
gem 'active_model_serializers', '~> 0.10.7'

# upload files easily
gem 'paperclip', '~> 5.1.0'

# background, delayed and scheduled jobs
gem 'resque', '~> 1.27.4'
gem 'resque-scheduler', '~> 4.3.1'

# add DB schema explanation at the top of models
gem 'annotate', '~> 2.7.2'

# Send emails with less effort
gem 'sendgrid-rails', '~> 3.1'

# Searching and filtering
gem 'ransack', '~> 1.8.7'

# send push notifications
gem 'pubnub', '~> 4.0.27'

# generate pdf files from html
gem 'wicked_pdf', '~> 1.1.0'

# a binary dependency for wicked_pdf
gem 'wkhtmltopdf-binary', '~> 0.12.3.1'

# create or open Excel files
gem 'spreadsheet', '~> 1.1.5'

# Roo implements read access for all common spreadsheet types and CSV
gem 'roo', '~> 2.7.1'

# This library extends Roo to add support for handling class Excel files, including:
# .xls files
# .xml files in the SpreadsheetML format (circa 2003)
gem 'roo-xls', '~> 1.1.0'

# Improve the rendering of HTML emails by making CSS inline
gem 'premailer-rails'

# This gem provides a simple interface to determine the representation of your model data
# that should be rendered in your API responses.
gem 'acts_as_api', '~> 1.0.1'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # preview emails
  gem 'letter_opener'

  gem 'faker', '~> 1.8.7'

  gem 'awesome_print', '~> 1.8.0'

  # debugging tool
  gem 'pry'
  gem 'pry-doc'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
