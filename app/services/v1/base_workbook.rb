# require 'tempfile'

class V1::BaseWorkbook
  def initialize
    @book = Spreadsheet::Workbook.new
    @sheets = []
  end

  def add_sheet(name)
    @sheets << @book.create_worksheet(name: name)
  end
end