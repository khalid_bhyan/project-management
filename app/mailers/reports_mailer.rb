class ReportsMailer < ApplicationMailer
  default from: 'info@123.com'

  def pdf_report(company, report)
    @company = company

    attachments['report.pdf'] = {
      mime_type: 'application/pdf',
      content: report
    }
    mail(to: @company.email, subject: 'Your pdf report')
  end
end
