class SupportMailer < ApplicationMailer
  default from: 'support@123.com'

  def password_reset(company)
    @company = company
    @company.regenerate_password_reset_token
    
    mail(to: @company.email, subject: 'Your password reset link')
  end

  def password_reset_alert(company)
    @company = company

    mail(to: @company.email, subject: 'Your password has been changed!')
  end
end
