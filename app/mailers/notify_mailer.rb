class NotifyMailer < ApplicationMailer
  default from: 'customers@123.com'

  def welcome(company)
    @company = company
    mail(to: @company.email, subject: 'Welcome!')
  end
end
