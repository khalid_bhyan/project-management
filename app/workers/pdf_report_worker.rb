class PdfReportWorker
  @queue = :reports

  def self.perform(html, file_name, disposition)
    report = WickedPdf.new.pdf_from_string(html)
    ReportsMailer.pdf_report(Company.find(18), report)
  end
end