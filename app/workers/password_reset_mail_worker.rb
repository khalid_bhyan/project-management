# send welcome mail to a user
class PasswordResetMailWorker
  def self.queue
    :mailers
  end

  def self.perform(company_id)
    company = Company.find(company_id)
    mail = SupportMailer.password_reset(company).deliver_now
  end
end