class PdfReportMailWorker
  @queue = :reports

  # Params:
  #   company_id(Integer):  The company to send email to
  #   data_class(String):   The Model class name of the filtered result(used to find by ids)
  #   data_ids(Array):      IDs of the filtered result
  #   template(String):     The PDF report template
  def self.perform(company_id, data_class, data_ids, template)
    company = Company.find(company_id)

    data = data_class.constantize.where(id: data_ids)
    report = V1::PdfBuilder.new(
               data, data_class.downcase.pluralize, template
             ).build

    ReportsMailer.pdf_report(company, report).deliver_now
  end
end