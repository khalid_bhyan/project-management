# send welcome mail to a user
class PasswordResetAlertMailWorker
  def self.queue
    :mailers
  end

  def self.perform(company_id)
    company = Company.find(company_id)
    SupportMailer.password_reset_alert(company).deliver_now
  end
end