# == Schema Information
#
# Table name: employee_projects
#
#  id          :integer          not null, primary key
#  employee_id :integer
#  project_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class EmployeeProject < ApplicationRecord
  belongs_to :employee
  belongs_to :project
end
