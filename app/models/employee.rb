# == Schema Information
#
# Table name: employees
#
#  id           :integer          not null, primary key
#  name         :string
#  joining_date :date
#  birth_date   :date
#  company_id   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Employee < ApplicationRecord
  acts_as_api
  include V1::EmployeeApi

  # Association
  belongs_to :company
  has_many :tasks, foreign_key: :assignee_id, dependent: :nullify

  has_many :employee_projects
  has_many :projects, through: :employee_projects

  # Validations
  validates_presence_of :name, :joining_date, :birth_date

  validate :birth_date_valid
  validate :joining_date_valid

  validate :age_greater_than_twenty, if: :birth_date
  validate :joining_date_cant_be_in_future, if: :joining_date

  private

  def age_greater_than_twenty
    unless self.birth_date < Time.now - 20.years
      errors.add(:birth_date, :age_20)
    end
  end

  def joining_date_cant_be_in_future
    unless self.joining_date <= Date.current
      errors.add(:joining_date, :joining_date_in_future)
    end
  end

  def birth_date_valid
    unless date_valid? self.birth_date
      errors.add(:birth_date, :invalid_birth_date)
    end
  end

  def joining_date_valid
    unless date_valid? self.joining_date
      errors.add(:joining_date, :invalid_joining_date)
    end
  end

  def date_valid?(date)
    Date.parse(date.to_s)
    true
  rescue ArgumentError
    false
  end

  def self.import(file)
    sheet = open_spreadsheet(file)
    header = sheet.row(1)

    (2..sheet.last_row).each do |i|
       row = Hash[[header, sheet.row(i)].transpose]       
       employee = find_by(id: row["id"].to_i) || new
       employee.attributes = row
       employee.save
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".xls" then Roo::Spreadsheet.open(file)
    when ".csv" then Roo::CSV.new(file.path)
    else raise "Uknown type"
    end
  end
end
