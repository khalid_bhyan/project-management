# == Schema Information
#
# Table name: companies
#
#  id                     :integer          not null, primary key
#  name                   :string
#  email                  :string(100)
#  password               :string
#  password_digest        :string
#  token                  :string
#  password_reset_token   :string
#  password_reset_sent_at :datetime
#  token_created_at       :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  username               :string(100)
#  logo_file_name         :string
#  logo_content_type      :string
#  logo_file_size         :integer
#  logo_updated_at        :datetime
#

class Company < ApplicationRecord
  has_secure_password

  has_secure_token
  has_secure_token :password_reset_token

  has_many :employees
  has_many :projects

  has_attached_file :logo, style: { small: '64x64', medium: '100x100', large: '500x500' }

  EMAIL_REGEX = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,63}/i
  USERNAME_REGEX = /[a-z0-9_]/i

  validates_presence_of :name, :username, :email

  validates_uniqueness_of :username, :email

  validates_length_of :username, :email, within: 6..100

  validates_format_of :username, with: USERNAME_REGEX

  validates_format_of :email, with: EMAIL_REGEX

  validates_confirmation_of :password

  validates_attachment :logo, presence: true
  do_not_validate_attachment_file_type :logo

  def destroy_token
    self.token = nil
    save
  end

  def destroy_password_token
    self.password_reset_token = nil
    save
  end
end
