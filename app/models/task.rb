# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  project_id  :integer
#  assignee_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  status      :string
#

class Task < ApplicationRecord
  acts_as_api
  include V1::TaskApi

  # Associations
  belongs_to :project
  belongs_to :employee, foreign_key: :assignee_id, optional: true

  enum status: { pending: "Pending", completed: "Completed" }

  # Validations
  validates_presence_of :name, :description, :status
  validate :no_completed_task_without_assignee, on: :create
  validate :task_and_assignee_must_be_in_same_project, if: :assignee_present?

  # Reset the assignee of the task
  def reset_assignee
    self.assignee_id = nil
    save
  end

  private

  def no_completed_task_without_assignee
    if self.completed? && assignee_not_present?
      errors.add(:status, :completed_status_no_assignee)
    end
  end

  def task_and_assignee_must_be_in_same_project
    unless self.employee.projects.ids.include?(self.project.id)
      errors.add(:assignee_id, :task_and_employee_same_project)
    end
  end
  
  def assignee_present?
    self.assignee_id.present?
  end

  def assignee_not_present?
    !assignee_present?
  end
end
