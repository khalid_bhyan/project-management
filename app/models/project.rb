# == Schema Information
#
# Table name: projects
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  company_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Project < ApplicationRecord
  acts_as_api
  include V1::ProjectApi

  # Associations
  belongs_to :company
  has_many :tasks, dependent: :destroy
  has_many :employee_projects
  has_many :employees, through: :employee_projects

  # Validations
  validates_presence_of :name, :description
end
