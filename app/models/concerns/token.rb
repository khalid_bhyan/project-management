module Token
    def generate_login_token
        self.token = generate_token
        self.token_created_at = Time.now
        save
        self.token
    end

    def generate_password_token
        self.password_reset_token = generate_token
        self.password_reset_sent_at = Time.now
        save
        self.password_reset_token
    end

    def reset_password(password)
        self.password_reset_token = nil
        self.password = password
        save
        self.password_reset_token
    end

    def reset_login_token
        self.token = nil
        save
    end

    private

    def generate_token
        SecureRandom.hex(10)
    end
end