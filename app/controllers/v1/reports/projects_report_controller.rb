class V1::Reports::ProjectsReportController < ApplicationController
  skip_before_action :authorize_user

  def show
    result = serialize_collection(
      Project.ransack(params[:q]).result, V1::Reports::ProjectsReportSerializer
    )
    projects = JSON.parse(result.to_json)

    respond_to do |format|
      format.json do
        success_response(projects)
      end

      format.pdf do
        report = V1::PdfBuilder.new(
                   projects, 'projects', 'v1/reports/projects_report/show'
                 ).build

        Resqeu.enqueue()
        send_data(report, filename: report_name(:pdf), type: :pdf, disposition: :attachment)
      end

      format.xls do
        # create workbook with worksheet
        report = Spreadsheet::Workbook.new
        sheet = report.create_worksheet name: 'Projects Information'

        # fill the sheet with data
        format = Spreadsheet::Format.new color: :blue
        # attrs = ['name', 'description', 'tasks count', 'completed tasks', 'assigned tasks']
        sheet.row(0).replace keys_human_names(projects[0]) if projects.any?

        projects.each_with_index do |project, i|
          sheet.row(i + 1).replace project.values
        end

        # write the workbook to string buffer then send data
        buffer = StringIO.new
        report.write buffer
        buffer.rewind
        
        send_data(buffer.read, filename: report_name(:xls), type: :xls, disposition: :attachment)
      end

      format.csv do
        csv = V1::CSVBuilder.new(
                true,
                keys_human_names(projects[0]),
                projects.each()
              ).build_from_hash_array

        send_data(csv, filename: report_name(:csv), type: :csv, disposition: :attachment)
      end

      format.html do
      end
    end
  end

  private

  def keys_human_names(hash)
    hash.keys.map { |k| k.to_s.sub("_", " ").titleize }
  end

  def report_name(type)
    "Projects Report.#{type.to_s}"
  end
end
