class V1::PasswordsController < ApplicationController
  skip_before_action :authorize_user, except: :update

  def update
    # check if the user is logged in
    # ask for his old password
    # if matched then update the password with the one he sent

    if @current_user.authenticate(params[:old_password])
      reset_password(@current_user)
    else
      json_response(nil, I18n.t("unauthorized"), false, 0, :unauthorized)
    end
  end

  def forgot
    # send an email with a link contains unique tokens
    company = Company.find_by!(email: params[:email])
    Resque.enqueue(PasswordResetMailWorker, company.id)
    json_response(nil, I18n.t("password_email_sent"), true, 1)
  end

  def reset
    # receive the token and compare it with the existing one
    company = Company.find_by!(password_reset_token: params[:token])
    reset_password(company)
    company.destroy_password_token
  end

  private

  def reset_password(company)
    company.update_attributes!(password_params)
    # send alert email after upadting/resetting password
    Resque.enqueue(PasswordResetAlertMailWorker, company.id)
    json_response(nil, I18n.t("password_reset"), true, 1)
  end

  def password_params
    params.permit(:password, :password_confirmation)
  end
end
