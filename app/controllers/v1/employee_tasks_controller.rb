class V1::EmployeeTasksController < ApplicationController
  before_action :set_employee

  def update
    @employee.tasks.replace(Task.where(id: params[:task_ids]))
    success_response(nil, I18n.t("updated"))
  end

  private

  def set_employee
    @employee = @current_user.employees.find(params[:employee_id])
  end
end
