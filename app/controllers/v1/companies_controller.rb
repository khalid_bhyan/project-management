class V1::CompaniesController < ApplicationController
  skip_before_action :authorize_user, only: :create

  def show
    data = serialize(@current_user, V1::CompanySerializer)
    json_response(data, "", true, 1)
  end

  def create
    company = Company.create!(company_params)
    Resque.enqueue(WelcomeMailWorker, company.id)

    json_response({ company: company, token: company.token },
                  I18n.t("registered"), true, 1, :created)
  end

  def update
    @current_user.update_attributes!(company_params)
    json_response(nil, I18n.t("updated"), true, 1)
  end

  private

  def company_params
    params.permit(:name, :username, :email, :password, :password_confirmation, :logo)
  end
end