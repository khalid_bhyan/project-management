class V1::EmployeesController < ApplicationController
  before_action :set_employee, except: [:index, :create, :import]
  
  def index
    employees = @current_user.employees.page(page).per(per_page)
    render_for_api :base, json: employees, root: :data, meta: success_meta
  end

  def show
    render_for_api :employee_with_tasks, json: @employee, root: :data, meta: success_meta
  end

  def create
    render_for_api :base, json: employee, root: :data, meta: success_meta
  end

  def update
    @employee.update_attributes!(employee_params)
    success_response(nil, I18n.t("updated"))
  end

  def destroy
    @employee.destroy!(employee_params)
    success_response(nil, I18n.t("deleted"))
  end

  def import
    Employee.import(params[:employees_file])
    success_response(nil, "Employees imported successfully")
  end

  private

  def set_employee
    @employee = @current_user.employees.find(params[:id])
  end

  def employee_params
    params.permit(:name, :joining_date, :birth_date, :company_id)
  end
end
