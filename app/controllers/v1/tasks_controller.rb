class V1::TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, except: [:index, :create]
  
  def index
    tasks = @project.tasks.page(page).per(per_page)
    render_for_api :base, json: tasks, root: :data, meta: success_meta
  end

  def show
    render_for_api :base, json: @task, root: :data, meta: success_meta
  end

  def create
    task = @project.tasks.create!(task_params)
    render_for_api :base, json: task, root: :data, meta: success_meta
  end

  def update
    @task.update_attributes!(task_params)
    success_response(nil, I18n.t("updated"))
  end

  def destroy
    @task.destroy!
    success_response(nil, I18n.t("deleted"))
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
  end

  def set_task
    @task = @project.tasks.find(params[:id])
  end

  def task_params
    params.permit(:name, :description, :status, :project_id, :assignee_id)
  end
end
