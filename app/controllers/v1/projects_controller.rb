class V1::ProjectsController < ApplicationController
  before_action :set_project, except: [:index, :create]
  
  def index
    projects = @current_user.projects.page(page).per(per_page)
    render_for_api :base, json: projects, root: :data, meta: success_meta
  end

  def show
    render_for_api :project_with_tasks, json: @project, root: :data, meta: success_meta
  end

  def create
    project = @current_user.projects.create!(project_params)
    render_for_api :base, json: project, root: :data, meta: success_meta
  end

  def update
    @project.update_attributes!(project_params)
    success_response(nil, I18n.t("updated"))
  end

  def destroy
    @project.destroy!(project_params)
    success_response(nil, I18n.t("deleted"))
  end

  private

  def set_project
    @project = @current_user.projects.find(params[:id])
  end

  def project_params
    params.permit(:name, :description)
  end
end
