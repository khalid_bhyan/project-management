class V1::ProjectEmployeesController < ApplicationController
  before_action :set_project

  def update
    employee_ids = params[:employee_ids]
    reset_tasks(@project.employees.where.not(id: employee_ids))
    @project.employees.replace(Employee.where(id: employee_ids))
    success_response(nil, I18n.t("updated"))
  end

  private

  def set_project
    @project = @current_user.projects.find(params[:project_id])
  end

  def reset_tasks(employees)
    employees.each do |e|
      e.tasks.each(&:reset_assignee)
    end
  end
end
