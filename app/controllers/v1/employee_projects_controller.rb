class V1::EmployeeProjectsController < ApplicationController
  before_action :set_employee

  def update
    project_ids = params[:project_ids]
    reset_tasks(@employee.projects.where.not(id: project_ids))
    @employee.projects.replace(Project.where(id: project_ids))
    success_response(nil, I18n.t("updated"))
  end

  private

  def set_employee
    @employee = @current_user.employees.find(params[:employee_id])
  end

  def reset_tasks(projects)
    projects.each do |p|
      p.tasks.where(employee: @employee).each(&:reset_assignee)
    end
  end
end
