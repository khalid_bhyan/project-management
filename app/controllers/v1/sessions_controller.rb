class V1::SessionsController < ApplicationController
  skip_before_action :authorize_user, only: :create

  def create
    # receive the token and match
    data = V1::Auth.authenticate(params[:login], params[:password])

    if data
      json_response(data, I18n.t("logged_in"), true, 1)
    else
      json_response(nil, I18n.t("invalid_login"), false, 0, :unprocessable_entity)
    end
  end

  def destroy
    @current_user.destroy_token
    json_response(nil, I18n.t("logged_out"), true, 1)
  end
end