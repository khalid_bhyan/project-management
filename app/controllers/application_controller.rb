class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include ActsAsApi::Rendering
  include V1::Response
  include V1::ErrorHandler
  include V1::Serializer

  before_action :set_locale
  before_action :authorize_user

  def page
    params[:page]
  end

  def per_page
    params[:per_page]
  end

  private

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def authorize_user
    token = V1::Auth.authorize(request.headers['Authorization'])
    @current_user = Company.find_by!(token: token)
  end
end
