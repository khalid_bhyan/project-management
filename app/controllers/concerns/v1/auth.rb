class V1::Auth
  def self.authenticate(login, password)
    # get user username/email and password, check if exists then generate a token
    company = Company.find_by(
        login.include?("@") ? {email: login} : {username: login}
    )

    if company && company.authenticate(password)
      company.regenerate_token
      { token: company.token }
    end
  end

  def self.authorize(auth_param)
    if auth_param
      return auth_param.split(" ").last
    end

    raise V1::ErrorHandler::MissingToken, ""
  end
end