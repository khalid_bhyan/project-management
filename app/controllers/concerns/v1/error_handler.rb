module V1::ErrorHandler
  extend ActiveSupport::Concern

  included do
    class NotAuthorized < StandardError; end
    class MissingToken  < StandardError; end
    class InvalidToken  < StandardError; end

    rescue_from V1::ErrorHandler::NotAuthorized,  with: :unauthorized_res
    rescue_from V1::ErrorHandler::MissingToken,   with: :missing_token
    rescue_from ActiveRecord::RecordNotFound,     with: :not_found
    rescue_from ActiveRecord::RecordInvalid,      with: :invalid_record
  end

  private

  def unauthorized_res
    err_response(nil, I18n.t("unauthorized"), :unauthorized)
  end

  def missing_token
    err_response(nil, I18n.t("missing_token"), :unauthorized)
  end

  def invalid_record(e)
    err_response(e.record.errors, "", :unprocessable_entity)
  end

  def invalid_argument(e)
    err_response(e, "", :unprocessable_entity)
  end

  def not_found(e)
    err_response(e, I18n.t("not_found"), :not_found)
  end

  private

  def err_response(data, message, status)
    base_response(data, message, false, 0, status)
  end
end