module V1::Response
  def success_response(data, message = "", status = :ok)
    base_response(data, message, true, 1, status)
  end

  def error_response(data, message = "", status = :unprocessable_entity)
    base_response(data, message, false, 0, status)
  end

  def success_meta(message = "", status = :ok)
    base_meta(message, true, status)
  end

  def error_meta(message = "", status = :unprocessable_entity)
    base_meta(message, false, status)
  end

  private

  def base_response(data, message = "", success = true, status = 1, http_status = :ok)
    res = {
        success: success,
        status: status,
        message: message,
        data: data
    }

    render json: res, status: http_status
  end

  def base_meta(message, success, status)
    {
        success: success,
        status: status,
        message: message
    }
  end
end