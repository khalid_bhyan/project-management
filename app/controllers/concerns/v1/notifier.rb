class V1::Notifier
  attr_reader :pubnub

  def initialize
    @pubnub ||= Pubnub.new(
      publish_key: Rails.application.secrets.pubnub[:publish],
      subscribe_key: Rails.application.secrets.pubnub[:subscribe]
    )
  end

  def push(channel, text)
    @pubnub.publish(
      channel: channel,
      message: { text: text }
    ) do |envelope|
      puts envelope.status
    end
  end
end