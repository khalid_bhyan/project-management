class V1::PdfBuilder
  def initialize(data, data_key, template, layout = nil)
    @html = ActionController::Base.new.render_to_string(
      layout: layout,
      template: template,
      assigns: {data_key => data}
    )
  end

  def build
    WickedPdf.new.pdf_from_string(@html)
  end
end