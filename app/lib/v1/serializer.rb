module V1::Serializer
  # short: only necessary data, full: all data related to object with all associations
  FORMS = {short: 0, full: 1}

  def serialize(object, serializer, scp = {})
    serializer.new(object, scope: scp)
  end

  def serialize_collection(collection, serializer, scp = {})
    ActiveModel::Serializer::CollectionSerializer.new(
      collection,
      { serializer: serializer, scope: scp }
    )
  end

  private

  def forms
    FORMS
  end
end