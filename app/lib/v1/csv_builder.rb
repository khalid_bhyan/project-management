require 'csv'

class V1::CSVBuilder
  def initialize(with_headers, attrs, data)
    @with_headers = with_headers
    @attrs = attrs
    @data = data
  end

  def build_from_collection
    @data = @data.map(&:attributes)
    build_from_hash_array
  end

  def build_from_hash_array
    CSV.generate(headers: @with_headers) do |csv|
      csv << @attrs unless @attrs.nil?
      @data.each { |d| csv << d.values }
    end
  end

  private
end