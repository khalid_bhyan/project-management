class V1::CompanySerializer < ActiveModel::Serializer
  attributes :name, :email, :logo
end
