class V1::EmployeeSerializer < ActiveModel::Serializer
  include V1::Serializer
  
  attributes :id, :name, :birth_date, :joining_date, :projects_count, :tasks_count
  attribute :tasks, if: :full?

  def projects_count
    object.projects.count
  end

  def tasks_count
    object.tasks.count
  end

  def tasks
    serialize_collection(object.tasks, V1::TaskSerializer)
  end

  def full?
    scope[:form] == forms[:full]
  end
end
