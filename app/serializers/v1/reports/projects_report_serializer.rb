class V1::Reports::ProjectsReportSerializer < ActiveModel::Serializer
  include V1::Serializer

  attributes :id, :name, :description, :tasks_count, :completed_tasks, :assigned_tasks
  attribute :tasks, if: :full?

  def tasks_count
    object.tasks.count
  end

  def completed_tasks
    object.tasks.where(status: :completed).count
  end

  def assigned_tasks
    object.tasks.where.not(assignee_id: nil).count
  end

  def tasks
    serialize_collection(object.tasks, V1::TaskSerializer)
  end

  def full?
    scope[:form] == forms[:full]
  end
end
