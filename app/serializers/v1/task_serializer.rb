class V1::TaskSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :status, :assignee_name, :project_name

  def assignee_name
    object.employee.name if object.employee.present?
  end

  def project_name
    object.project.name
  end

  def status
    Task.human_attribute_name(object.status)
  end
end
