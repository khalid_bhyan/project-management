module V1::TaskApi
  extend ActiveSupport::Concern

  included do
    api_accessible :base do |t|
      t.add :id
      t.add :name
      t.add :description
      t.add :task_status
      t.add :assignee_name
      t.add :project_name
    end

    api_accessible :employee_with_tasks, extend: :base

    api_accessible :project_with_tasks, extend: :base

    def assignee_name
      employee.name if employee.present?
    end

    def project_name
      project.name
    end

    def task_status
      Task.human_attribute_name(status)
    end
  end
end