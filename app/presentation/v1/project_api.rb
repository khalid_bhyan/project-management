module V1::ProjectApi
  extend ActiveSupport::Concern

  included do
    api_accessible :base do |t|
      t.add :id
      t.add :name
      t.add :description
      t.add :tasks_count
      t.add :completed_tasks
      t.add :assigned_tasks
    end

    api_accessible :project_with_tasks, extend: :base do |t|
      t.add :tasks
    end

    def tasks_count
      tasks.count
    end

    def completed_tasks
      tasks.where(status: :completed).count
    end

    def assigned_tasks
      tasks.where.not(assignee_id: nil).count
    end
  end
end