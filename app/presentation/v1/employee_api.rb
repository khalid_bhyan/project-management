module V1::EmployeeApi
  extend ActiveSupport::Concern

  included do
    api_accessible :base do |t|
      t.add :id
      t.add :name
      t.add :birth_date
      t.add :joining_date
      t.add :projects_count
      t.add :tasks_count
    end

    api_accessible :employee_with_tasks, extend: :base do |t|
      t.add :tasks
    end

    def projects_count
      projects.count
    end

    def tasks_count
      tasks.count
    end
  end
end