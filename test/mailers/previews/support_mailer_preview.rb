# Preview all emails at http://localhost:3000/rails/mailers/password_reset_mailer
class SupportMailerPreview < ActionMailer::Preview
    def reset_password
        SupportMailer.reset_password(Company.first)
    end
end
