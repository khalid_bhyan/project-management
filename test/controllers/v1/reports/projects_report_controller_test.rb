require 'test_helper'

class V1::Reports::ProjectsReportControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get v1_reports_projects_report_show_url
    assert_response :success
  end

end
