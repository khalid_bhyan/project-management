require 'test_helper'

class V1::EmployeeTasksControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get employee_tasks_index_url
    assert_response :success
  end

  test "should get update" do
    get employee_tasks_update_url
    assert_response :success
  end

end
