class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :name
      t.string :description
      t.string :status
      t.integer :project_id, foreign_key: true
      t.integer :assignee_id, foreign_key: true

      t.timestamps
    end
  end
end
