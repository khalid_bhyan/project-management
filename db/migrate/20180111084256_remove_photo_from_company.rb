class RemovePhotoFromCompany < ActiveRecord::Migration[5.1]
  def change
    remove_column 'companies', 'photo_file_name'
    remove_column 'companies', 'photo_content_type'
    remove_column 'companies', 'photo_file_size'
    remove_column 'companies', 'photo_updated_at'
  end
end
