class AddIndexesTasks < ActiveRecord::Migration[5.1]
  def change
    add_index :tasks, [:assignee_id, :project_id]
  end
end
