class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.datetime :joining_date
      t.datetime :birth_date
      t.integer :company_id

      t.timestamps
    end
  end
end
