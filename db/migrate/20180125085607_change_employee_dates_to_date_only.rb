class ChangeEmployeeDatesToDateOnly < ActiveRecord::Migration[5.1]
  def change
    change_column :employees, :birth_date, :date
    change_column :employees, :joining_date, :date
  end
end
