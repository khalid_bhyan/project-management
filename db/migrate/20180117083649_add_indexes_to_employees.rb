class AddIndexesToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_index :employees, :company_id
  end
end
