class AddIndexesToProjects < ActiveRecord::Migration[5.1]
  def change
    add_index :projects, :company_id
  end
end
