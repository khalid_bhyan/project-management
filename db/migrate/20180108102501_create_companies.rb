class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :email, limit: 100
      t.string :password
      t.string :password_digest
      t.string :token
      t.string :password_reset_token
      t.datetime :password_reset_sent_at
      t.datetime :token_created_at

      t.timestamps
    end
  end
end
