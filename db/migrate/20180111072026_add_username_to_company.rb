class AddUsernameToCompany < ActiveRecord::Migration[5.1]
  def change
    add_column 'companies', 'username', :string, limit: 100
  end
end
