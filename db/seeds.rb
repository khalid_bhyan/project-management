# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

3.times do |i|
  emp = Employee.create!(name: Faker::Name.name,
      joining_date: Time.now, birth_date: Time.new("01-01-1996"), company_id: 18)

  project = Project.create!(name: Faker::StarWars.character, description: "this is description #{i}", company_id: 18)

  emp.projects << project

  Task.create!(name: "Task #{i}",
      description: "it's the task #{i}",
      status: "Pending", assignee_id: project.employees[0].id, project_id: project.id)
end
